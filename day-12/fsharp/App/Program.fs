﻿let simpleText = """start-A
start-b
A-c
A-b
b-d
A-end
b-end"""

let sampleText = """dc-end
HN-start
start-kj
dc-start
dc-HN
LN-dc
HN-end
kj-sa
kj-HN
kj-dc"""

let largerText = """fs-end
he-DX
fs-he
start-DX
pj-DX
end-zg
zg-sl
zg-pj
pj-he
RW-he
fs-DX
pj-RW
zg-RW
start-pj
he-WI
zg-he
pj-fs
start-RW"""

let puzzleText = """VJ-nx
start-sv
nx-UL
FN-nx
FN-zl
end-VJ
sv-hi
em-VJ
start-hi
sv-em
end-zl
zl-em
hi-VJ
FN-em
start-VJ
jx-FN
zl-sv
FN-sv
FN-hi
nx-end"""

let START = "start"
let END = "end"
let SMALL_CAVE_PATTERN = "[a-z]+"

type Graph = Map<string, Set<string>>

let parse (text: string) : Graph =
    text.Split("\n")
        |> Seq.map (fun line -> line.Trim().Split("-"))
        |> Seq.collect (fun pair -> [| pair |> Seq.ofArray; Seq.rev pair |])
        |> Seq.groupBy (fun x -> Seq.head x)
        |> Map.ofSeq
        |> Map.map (fun k v -> (Seq.map (fun x -> Seq.tail x |> Seq.head) v) |> Set.ofSeq)

let joinPath (parts: seq<string>): string =
    parts |> Seq.rev |> String.concat "-"

let rec traversePaths (graph: Graph) (seen: Set<string>) (path: List<string>): Set<string> =
    let fromNode = Seq.head path
    if fromNode = END then [| joinPath path |] |> Set.ofSeq else (
        let fromNodeChildren = graph[fromNode]
        let candidates = fromNodeChildren - seen
        if candidates.IsEmpty then Set.empty else (
            let newSeen = if fromNode = fromNode.ToLower() then seen.Add(fromNode) else seen
            let paths = Seq.collect (fun c -> traversePaths graph newSeen (c::path)) candidates
            paths |> Set.ofSeq
        )
    )

let isLower (str: string): bool =
    str = str.ToLower()

let simpleGraph = parse simpleText
let sampleGraph = parse sampleText
let largerGraph = parse largerText
let puzzleGraph = parse puzzleText

let startSet = ([|START|] |> Set.ofSeq)
let startPaths = ([|START|] |> List.ofSeq)

let simpleGraphPaths = (traversePaths simpleGraph startSet startPaths)
let sampleGraphPaths = (traversePaths sampleGraph startSet startPaths)
let largerGraphPaths = (traversePaths largerGraph startSet startPaths)
let puzzleGraphPaths = (traversePaths puzzleGraph startSet startPaths)

printfn "simple %i" (simpleGraphPaths.Count)
printfn "sample %i" (sampleGraphPaths.Count)
printfn "larger %i" (largerGraphPaths.Count)
printfn "puzzle %i" (puzzleGraphPaths.Count)

exception Debug of string

let rec traversePaths2 (graph: Graph) (seen: Map<string, int>) (path: List<string>): Set<string> =
    // printfn "path %s" (joinPath path)
    let fromNode = Seq.head path
    if fromNode = END then [| joinPath path |] |> Set.ofSeq else (
        let fromNodeChildren = graph[fromNode]
        let hasAnotherLowerBeenVisitedTwice = (Map.filter (fun k _ -> isLower k) seen |> Map.values |> Seq.sortDescending |> Seq.head) >= 2
        if isLower fromNode && hasAnotherLowerBeenVisitedTwice && (seen.ContainsKey fromNode) then Set.empty else (
            let candidates = fromNodeChildren - Set.ofSeq [|START|]
            if candidates.IsEmpty then Set.empty else (
                let newSeen = if isLower fromNode then seen.Add(fromNode, 1 + (seen.TryFind(fromNode) |> Option.defaultValue(0))) else seen
                let paths = Seq.collect (fun c -> traversePaths2 graph newSeen (c::path)) candidates
                paths |> Set.ofSeq
            )
        )
    )

let startMap = Map.ofSeq([| (START, 0) |])

let simpleGraphPaths2 = (traversePaths2 simpleGraph startMap startPaths)
let sampleGraphPaths2 = (traversePaths2 sampleGraph startMap startPaths)
let puzzleGraphPaths2 = (traversePaths2 puzzleGraph startMap startPaths)

printfn "simple %i" (simpleGraphPaths2.Count)
printfn "sample %i" (sampleGraphPaths2.Count)
printfn "puzzle %i" (puzzleGraphPaths2.Count)