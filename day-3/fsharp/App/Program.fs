﻿open System

let filePath = "../../input"
// let filePath = "../../sample"
let input = System.IO.File.ReadLines(filePath)

let countByColumn (input: seq<String>) =
    input |> Seq.map Seq.toList |> Seq.transpose |> Seq.map (Seq.countBy id) |> Seq.map (Seq.sortBy snd)

let bitsToInt (bits: seq<char>) =
    String.Concat bits |> (fun bits -> Convert.ToInt32(bits, 2))

let countsByColumn = input |> Seq.map (Seq.toList) |> Seq.transpose |> Seq.map (Seq.countBy id) |> Seq.map (Seq.sortBy snd)

let gammaBits = countsByColumn |> Seq.map (Seq.last) |> Seq.map fst
let epsilonBits = countsByColumn |> Seq.map (Seq.head) |> Seq.map fst

let gamma = gammaBits |> bitsToInt
let epsilon = epsilonBits |> bitsToInt

printfn "%A" countsByColumn
printfn "Gamma bits: %s" (String.Concat gammaBits)
printfn "Epsilon bits: %s" (String.Concat epsilonBits)
printfn "Gamma: %i" gamma
printfn "Epsilon: %i" epsilon
printfn "%i\n" (gamma * epsilon)

printfn "\nPART 2\n"

let filterRatingsByPredicate inputs (countBasedSelector: seq<char * int> -> char * int) position defaultTarget =
    let countsByColumn = countByColumn inputs
    let targetColumnCounts = countsByColumn |> Seq.skip position |> Seq.head
    // Handle ties
    let target = match targetColumnCounts |> Seq.toList with
        | [(_, a); (_, b)] when a = b -> defaultTarget
        | _ -> targetColumnCounts |> countBasedSelector |> fst
    inputs |> Seq.filter (fun x -> x.Chars(position) = target)

let rec computeRating input countBasedSelector position defaultTarget =
    if Seq.length input <= 1 then Seq.head input else computeRating (filterRatingsByPredicate input countBasedSelector position defaultTarget) countBasedSelector (position + 1) defaultTarget

let oxygenGeneratorRating = computeRating  input Seq.last 0 '1' |> Seq.toList |> bitsToInt
let co2ScrubberRating = computeRating input Seq.head 0 '0' |> Seq.toList |> bitsToInt

printfn "Oxygen Generator Rating: %i" oxygenGeneratorRating
printfn "CO2 Scrubber Rating: %i" co2ScrubberRating
printfn "%i" (oxygenGeneratorRating * co2ScrubberRating)