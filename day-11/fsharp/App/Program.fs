﻿let simpleText = """11111
19991
19191
19991
11111"""

let sampleText = """5483143223
2745854711
5264556173
6141336146
6357385478
4167524645
2176841721
6882881134
4846848554
5283751526"""

let puzzleText = """4341347643
5477728451
2322733878
5453762556
2718123421
4237886115
5631617114
2217667227
4236581255
4482627641"""

type Board = array<array<int>>
type Position = int*int

let parse (text: string):Board =
    text.Split("\n") |> Seq.map (fun x -> x.Trim() |> Seq.map (string) |> Seq.map (int) |> Array.ofSeq) |> Array.ofSeq

let allPositions (board: Board): seq<Position> =
    seq {
        for i in 0 .. (board.Length - 1) do
            for j in 0 .. (board[0].Length - 1) do
                yield (i, j)
    }

let inc (board: Board) (positions: seq<Position>):unit =
    for position in positions do
        let a,b = position
        board[a][b] <- board[a][b] + 1

let reset (board: Board) (positions: seq<Position>):unit =
    for position in positions do
        let a,b = position
        board[a][b] <- 0

let adjacent (board: Board) (position: Position): seq<Position> =
    let row, col = position
    let lowerRow = max 0 (row - 1)
    let upperRow = min (board.Length - 1) (row + 1)
    let lowerCol = max 0 (col - 1)
    let upperCol = min (board[0].Length - 1) (col + 1)
    seq {
        for i in lowerRow .. upperRow do
            for j in lowerCol .. upperCol do
                if not (i = row && j = col) then (i, j)
    }

let printBoardState (board: Board) (msg: string): unit =
    printfn "BOARD STATE %s" msg
    for row in board do
        printfn "%A" row
    printfn "END %s" msg

let step (board: Board): Set<Position> =
    // printBoardState board "Start..."
    inc board (allPositions board)
    let rec countFlashes (flashers: Set<Position>) (board: Board): Set<Position> =
        let flasherCandidates = allPositions board |> Seq.filter (fun (i,j) -> board[i][j] > 9) |> Set.ofSeq
        let unaccountedFlashers = flasherCandidates - flashers
        if unaccountedFlashers.IsEmpty then flashers else (
            // Increment neighbors of unaccountedFlashers
            Seq.collect (adjacent board) unaccountedFlashers |> (inc board)
            // DEBUG
            // printBoardState board "Iter"
            // Recurse, adding unaccountedFlashers to flashers set
            countFlashes (flashers + unaccountedFlashers) board
        )
    let finalFlashes = countFlashes Set.empty board
    let positionsToReset = (allPositions board) |> Seq.filter (fun (i,j) -> board[i][j] > 9)
    reset board positionsToReset
    // printBoardState board "After reset"
    finalFlashes

// For more information see https://aka.ms/fsharp-console-apps
let simpleBoard = parse simpleText
let sampleBoard = parse sampleText
let puzzleBoard = parse puzzleText

// let flashes = step simpleBoard
let flashes = {1..100} |> Seq.map (fun _ -> step puzzleBoard)
printfn "Flashes %i" (Seq.sum (Seq.map (fun (c:Set<Position>) -> c.Count) flashes))

let untilAllFlash (board: Board):int =
    (Seq.initInfinite id |> Seq.filter (fun i -> (
        let flashes = step board
        if flashes.Count = 100 then true else false
    )) |> Seq.head) + 1

let puzzleBoard2 = parse puzzleText
let sampleBoard2 = parse sampleText
printfn "Sample Board - All flash at %i" (untilAllFlash sampleBoard2)
printfn "Puzzle Board - All flash at %i" (untilAllFlash puzzleBoard2)