open System

let rec computeGenerations (previousState: int64 array) (generations: int) =
    if generations = 0 then previousState else (
        let newState = [|
            previousState[8];
            previousState[0];
            previousState[1] + previousState[8];
            previousState[2];
            previousState[3];
            previousState[4];
            previousState[5];
            previousState[6];
            previousState[7];
        |]
        computeGenerations newState (generations - 1)
    )

let computeFinalGeneration (initialState: seq<int>) (generations: int) =
    
    let stateMap = initialState |> Seq.countBy id |> Map.ofSeq
    let stateArray = [| 0L; 0L; 0L; 0L; 0L; 0L; 0L; 0L; 0L; |]
    for i in seq {0 .. 8 } do (
        let nextValue = match stateMap.TryFind(8 - i) with
            | Some x -> x
            | _ -> 0
        stateArray.[i] <- nextValue
    )
    computeGenerations stateArray generations |> Seq.map Convert.ToInt64 |> Seq.sum

[<EntryPoint>]
let main argv =
    let sampleInit = [ 3; 4; 3; 1; 2 ]
    let init = [ 3; 3; 2; 1; 4; 1; 1; 2; 3; 1; 1; 2; 1; 2; 1; 1; 1; 1; 1; 1; 4; 1; 1; 5; 2; 1; 1; 2; 1; 1; 1; 3; 5; 1; 5; 5; 1; 1; 1; 1; 3; 1; 1; 3; 2; 1; 1; 1; 1; 1; 1; 4; 1; 1; 1; 1; 1; 1; 1; 4; 1; 3; 3; 1; 1; 3; 1; 3; 1; 2; 1; 3; 1; 1; 4; 1; 2; 4; 4; 5; 1; 1; 1; 1; 1; 1; 4; 1; 5; 1; 1; 5; 1; 1; 3; 3; 1; 3; 2; 5; 2; 4; 1; 4; 1; 2; 4; 5; 1; 1; 5; 1; 1; 1; 4; 1; 1; 5; 2; 1; 1; 5; 1; 1; 1; 5; 1; 1; 1; 1; 1; 3; 1; 5; 3; 2; 1; 1; 2; 2; 1; 2; 1; 1; 5; 1; 1; 4; 5; 1; 4; 3; 1; 1; 1; 1; 1; 1; 5; 1; 1; 1; 5; 2; 1; 1; 1; 5; 1; 1; 1; 4; 4; 2; 1; 1; 1; 1; 1; 1; 1; 3; 1; 1; 4; 4; 1; 4; 1; 1; 5; 3; 1; 1; 1; 5; 2; 2; 4; 2; 1; 1; 3; 1; 5; 5; 1; 1; 1; 4; 1; 5; 1; 1; 1; 4; 3; 3; 3; 1; 3; 1; 5; 1; 4; 2; 1; 1; 5; 1; 1; 1; 5; 5; 1; 1; 2; 1; 1; 1; 3; 1; 1; 1; 2; 3; 1; 2; 2; 3; 1; 3; 1; 1; 4; 1; 1; 2; 1; 1; 1; 1; 3; 5; 1; 1; 2; 1; 1; 1; 4; 1; 1; 1; 1; 1; 2; 4; 1; 1; 5; 3; 1; 1; 1; 2; 2; 2; 1; 5; 1; 3; 5; 3; 1; 1; 4; 1; 1; 4 ]
    let smallGenerations = 18
    let generations = 80
    let longGenerations = 256

    printfn "%i" (computeFinalGeneration sampleInit generations)
    printfn "%i" (computeFinalGeneration init generations)
    printfn "%i" (computeFinalGeneration sampleInit longGenerations)
    printfn "%i" (computeFinalGeneration init longGenerations)

    0 // return an integer exit code