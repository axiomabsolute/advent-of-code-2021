# Day 0

Two solutions are provided, in Julia and F#.

## Julia

The solution assumes that Julia is installed globally and that the following packages are installed (via interactive shell):

```
using Pkg
Pkg.add("CSV")
Pkg.add("DataFrames")
```

Navigate to the `day-0` directory and run `julia jl/day0.jl`.

## F#

Assumes that the dotnet core CLI is installed.

Navigate to the `day-0/fsharp/App` and run `dotnet run`.
