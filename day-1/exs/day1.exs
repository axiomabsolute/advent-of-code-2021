defmodule DayZero do
  def safeparse(s) do
    case Integer.parse(s) do
      {i, ""} -> i
      _ -> throw("Unable to parse input")
    end
  end

  def read_data() do
    File.read!("input") |>
      String.split |>
      Enum.map(&String.trim/1) |>
      Enum.map(&DayZero.safeparse/1)
  end

  def count_increasing(seq) do
    seq |> Enum.chunk_every(2, 1, :discard) |> Enum.count( (fn ([a, b]) -> a < b end) )
  end
end

data = DayZero.read_data()

# Part 1
data |> DayZero.count_increasing() |> IO.puts

# Part 2
data |> Enum.chunk_every(3, 1, :discard) |> Enum.map( &Enum.sum/1 ) |> DayZero.count_increasing |> IO.puts
