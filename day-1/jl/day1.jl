using CSV
using DataFrames

input_data_file_name = "./input"

function read_data()
    reader = CSV.File(input_data_file_name, header=false)
    df = DataFrames.DataFrame(reader)
    return df.Column1
end

"""
Given a collection of numeric values, count increasing overlapping pairs
"""
function part_1(pings)
    diffs = pings[2:end] .- pings[1:end-1]
    return sum(diffs .> 0)
end

"""
Given a collection of values, create overlapping windows of a given size
"""
function create_window(pings, window_size)
    slices = [pings[x:end] for x=1:window_size]
    return zip(slices...)
end

function part_2(pings)
    triples = [triple for triple=create_window(pings, 3)]
    # Transform original input to creating successive sums of 3 values
    measures = [sum(triple) for triple=triples]
    return part_1(measures)
end

inputData = read_data()

println(part_1(inputData))

println(part_2(inputData))