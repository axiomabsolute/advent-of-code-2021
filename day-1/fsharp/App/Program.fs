let countIncreasing seq =
    Seq.windowed 2 seq |>
        Seq.map (fun p -> if p.[0] < p.[1] then 1 else 0) |>
        Seq.sum

let tripleSum seq: (seq<int>) =
    Seq.windowed 3 seq |>
        Seq.map Seq.sum

[<EntryPoint>]
let main argv =
    let filePath = "../../input"

    let input = System.IO.File.ReadLines(filePath) |> Seq.map int

    printfn "%i" (countIncreasing input)
    printfn "%i" (input |> tripleSum |> countIncreasing)

    0 // return an integer exit code