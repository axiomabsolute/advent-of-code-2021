﻿open System;

let yPositions (velocity: int) = Seq.unfold (fun (pos,velocity) -> Some (pos+velocity, (pos+velocity, velocity-1))) (0,velocity)

let takeUntilPredicate (farthest: int) =
    // Given a maximum absolute range and the sign of that range, create a filter predicate
    // that takes from positions until we're guaranteed to be past the fartest point, or until
    // the position has stalled
    let predicate (arr: int array): bool =
        let p = arr[0]
        let c = arr[1]
        if farthest < 0 then (
            if c < farthest then false else true
        ) else (
            if c > farthest then false else true
        )
    predicate

let findIntersections (closer: int) (farther: int) (positions: seq<int>) =
    let positions = Seq.windowed 2 positions |> Seq.takeWhile (takeUntilPredicate farther) |> Seq.map (Array.last)
    positions |> Seq.filter (fun p -> p <= closer)

let hasIntersections (closer: int) (farther: int) (positions: seq<int>) = 
    not (Seq.isEmpty (findIntersections closer farther positions))

let printPositions (poss: int seq): string =
    (Seq.map (sprintf "%i") poss |> String.concat ", ")

let partOne (closer: int) (farther: int): int =
    // Picked the range here by trial and error.. keep re-running until "Highest V" isn't the top of the range.
    let velocitiesThatIntersect = {1 .. 1000} |> Seq.map (fun i -> (i, hasIntersections closer farther (yPositions i))) |> Seq.filter (fun (i,p) -> p) |> Seq.map fst
    let highestVelocity = Seq.last velocitiesThatIntersect
    printfn "Highest V: %i" highestVelocity
    {1 .. highestVelocity} |> Seq.sum

let xPositions (velocity: int) = Seq.unfold (fun (p,v) -> let newVelocity = if v >= 1 then v-1 else 0 in if v = 0 then None else Some (p+v, (p+v, newVelocity))) (0,velocity)




// Oooh boy, this took WAY too long

type SeriesValue<'a> = {
    InitialValue: 'a;
    Time: int;
    Value: 'a;
}

let yPositionSeries (v0: int): seq<SeriesValue<int>> =
    let positions = Seq.unfold (fun (pos,velocity) -> Some (pos+velocity, (pos+velocity, velocity-1))) (0, v0)
    Seq.mapi (fun i p -> {InitialValue = v0; Time = i; Value = p}) positions;

let xPositionSeries (v0: int) (highestT: int): seq<SeriesValue<int>> =
    let positions = Seq.unfold (fun (p,v) -> let newVelocity = if v >= 1 then v-1 else 0 in Some (p+v, (p+v, newVelocity))) (0,v0)
    Seq.mapi (fun i p -> {InitialValue = v0; Time = i; Value = p}) positions |>
        Seq.takeWhile (fun sv -> sv.Time <= highestT)

let pastY (fartherY: int) (seriesValue: SeriesValue<int>) =
    seriesValue.Value < fartherY

let partTwo (closerX: int) (fartherX: int) (closerY: int) (fartherY: int): seq<int*int> =
    let xRange = {0 .. 200}
    let yRange = {-115 .. 115}

    // Map from time to initial y-velocities that intersect at that time
    let yIntersections = seq {
        for v0 in yRange do
            let rawPositions = yPositionSeries v0
            let filteredPositions = rawPositions |> Seq.takeWhile (fun s -> s |> (pastY fartherY) |> not) |> Seq.filter (fun sv -> closerY >= sv.Value && sv.Value >= fartherY)
            for p in filteredPositions do
                yield p
    }
    let tToY0Intersections = yIntersections |> Seq.groupBy (fun sv -> sv.Time) |> Map.ofSeq |> Map.map (fun k v -> Seq.map (fun sv -> sv.InitialValue) v |> Set.ofSeq)

    // Find highest t that a Y target can possibly intersect
    let highestT = Seq.max (Map.keys tToY0Intersections)

    // Map from time to initial x-velocities that intersect at that time
    let xIntersections = seq {
        for v0 in xRange do
            let rawPositions = xPositionSeries v0 highestT
            let filteredPositions = rawPositions |> Seq.filter (fun sv -> closerX <= sv.Value && sv.Value <= fartherX)
            for p in filteredPositions do
                yield p
    }
    let tToX0Intersections = xIntersections |> Seq.groupBy (fun sv -> sv.Time) |> Map.ofSeq |> Map.map (fun k v -> Seq.map (fun sv -> sv.InitialValue) v |> Set.ofSeq)

    // Intersect the keys; for each time in the intersection, produce the cartesian product of initial velocities at that time
    let yIntersectionTimes = Map.keys tToY0Intersections |> Set.ofSeq
    let xIntersectionTimes = Map.keys tToX0Intersections |> Set.ofSeq
    let sharedIntersectionTimes = Set.intersect yIntersectionTimes xIntersectionTimes

    // Cartesian product
    seq {
        for time in sharedIntersectionTimes do
            let initialXVelocities = tToX0Intersections.[time]
            let initialYVelocities = tToY0Intersections.[time]
            for x in initialXVelocities do
                for y in initialYVelocities do
                    yield (x, y)
    } |> Seq.distinct

// Part one
printfn "%i" (partOne -75 -114)

// Part two
let allInintialVelocitiesThatIntersect = partTwo 153 199 -75 -114
printfn "%i" (Seq.length allInintialVelocitiesThatIntersect)
