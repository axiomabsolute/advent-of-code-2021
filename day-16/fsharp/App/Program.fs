﻿open System;;
open System.Collections;;

let samples = Map.ofSeq [|
    "8A004A801A8002F478", 16;
    "620080001611562C8802118E34", 12;
    "C0015000016115A2E0802F182340", 23;
    "A0016C880162017C3686B18A3D4780", 31;
|]

let puzzle = """420D50000B318100415919B24E72D6509AE67F87195A3CCC518CC01197D538C3E00BC9A349A09802D258CC16FC016100660DC4283200087C6485F1C8C015A00A5A5FB19C363F2FD8CE1B1B99DE81D00C9D3002100B58002AB5400D50038008DA2020A9C00F300248065A4016B4C00810028003D9600CA4C0084007B8400A0002AA6F68440274080331D20C4300004323CC32830200D42A85D1BE4F1C1440072E4630F2CCD624206008CC5B3E3AB00580010E8710862F0803D06E10C65000946442A631EC2EC30926A600D2A583653BE2D98BFE3820975787C600A680252AC9354FFE8CD23BE1E180253548D057002429794BD4759794BD4709AEDAFF0530043003511006E24C4685A00087C428811EE7FD8BBC1805D28C73C93262526CB36AC600DCB9649334A23900AA9257963FEF17D8028200DC608A71B80010A8D50C23E9802B37AA40EA801CD96EDA25B39593BB002A33F72D9AD959802525BCD6D36CC00D580010A86D1761F080311AE32C73500224E3BCD6D0AE5600024F92F654E5F6132B49979802129DC6593401591389CA62A4840101C9064A34499E4A1B180276008CDEFA0D37BE834F6F11B13900923E008CF6611BC65BCB2CB46B3A779D4C998A848DED30F0014288010A8451062B980311C21BC7C20042A2846782A400834916CFA5B8013374F6A33973C532F071000B565F47F15A526273BB129B6D9985680680111C728FD339BDBD8F03980230A6C0119774999A09001093E34600A60052B2B1D7EF60C958EBF7B074D7AF4928CD6BA5A40208E002F935E855AE68EE56F3ED271E6B44460084AB55002572F3289B78600A6647D1E5F6871BE5E598099006512207600BCDCBCFD23CE463678100467680D27BAE920804119DBFA96E05F00431269D255DDA528D83A577285B91BCCB4802AB95A5C9B001299793FCD24C5D600BC652523D82D3FCB56EF737F045008E0FCDC7DAE40B64F7F799F3981F2490"""

let hexToBitMap = Map.ofSeq [
    '0', [false; false; false; false; ];
    '1', [false; false; false; true; ];
    '2',[false; false; true; false; ];
    '3',[false; false; true; true; ];
    '4',[false; true; false; false; ];
    '5',[false; true; false; true; ];
    '6',[false; true; true; false; ];
    '7',[false; true; true; true; ];
    '8',[true; false; false; false; ];
    '9',[true; false; false; true; ];
    'A',[true; false; true; false; ];
    'B',[true; false; true; true; ];
    'C',[true; true; false; false; ];
    'D',[true; true; false; true; ];
    'E',[true; true; true; false; ];
    'F',[true; true; true; true; ];
]

type Header = {
    Version: int64
    Tag: int64
}

type Literal = {
    Header: Header
    Value: int64
} and Operator = {
    Header: Header
    Operands: list<Node>
} and Node = LiteralNode of Literal | OperatorNode of Operator

let inputToBitArray (hexString: string): bool array =
    hexString.ToCharArray() |> Seq.collect (fun c -> hexToBitMap.[c]) |> Seq.toArray

let bitsToInt (bin: bool array): int =
    let binaryString = Seq.map (fun i -> if i then "1" else "0") bin |> String.concat ""
    Convert.ToInt32(binaryString, 2)

let bitsToInt64 (bin: bool array): int64 =
    let binaryString = Seq.map (fun i -> if i then "1" else "0") bin |> String.concat ""
    Convert.ToInt64(binaryString, 2)

let parseHeader (input: bool array): Header*array<bool> =
    let versionBits = input[0..2]
    let tagBits = input[3..5]
    {Version = bitsToInt64 versionBits; Tag = bitsToInt64 tagBits},input[6..]

let rec countSegments (runningCount: int) (rest: bool array): int =
    if not rest.[0] then runningCount + 1 else countSegments (runningCount + 1) (rest[5..])

let rec parseLiteralBody (input: bool array): int64*array<bool> =
    let totalSegments = countSegments 0 input
    let baseSegmentsLength = 5 * totalSegments
    let fullSegmentWithoutSegmentHeaders = Seq.take baseSegmentsLength input |> Seq.chunkBySize 5 |> Seq.map (Seq.skip 1) |> Seq.collect id |> Seq.toArray
    let literalValue = bitsToInt64 (fullSegmentWithoutSegmentHeaders)
    let rest = input[baseSegmentsLength..]
    literalValue,rest
and parseLiteral (header: Header) (input: bool array): Literal*array<bool> =
    let value, rest = parseLiteralBody input
    let lit = {Header = header; Value = value}
    lit, rest
and parseOperandsByBitLength (input: bool array): list<Node>*array<bool> =
    let totalBitLengthInBits = input[..14]
    let totalBitLength = bitsToInt totalBitLengthInBits
    let operandBits = input[15..15+totalBitLength-1]

    let rec parseAllOperands (input: bool array) (acc: Node list): Node list =
        if Array.isEmpty input then acc else (
            let literal, rest = parsePacket input
            parseAllOperands rest (literal :: acc)
        )

    let operands = parseAllOperands operandBits List.empty
    let rest = input[15+totalBitLength..]
    (List.rev operands),rest
and parseOperandsBySubPacketCount (input: bool array): list<Node>*array<bool> =
    let operandCountBits = input[..10]
    let operandCount = bitsToInt operandCountBits
    let mutable operandRest = input[11..]
    let operands = seq {
        for _ in 1..operandCount do
            let lit, rest = parsePacket operandRest
            operandRest <- rest
            yield lit
    }
    (operands |> Seq.toList), operandRest
and parseOperatorBody (input: bool array): list<Node>*array<bool> =
    let lengthTypeId = input.[0]
    let operatorRest = Array.tail input
    let literals, rest = match lengthTypeId with
    | false -> parseOperandsByBitLength operatorRest
    | true -> parseOperandsBySubPacketCount operatorRest
    literals, rest
and parseOperator (header: Header) (input: bool array): Operator*array<bool> =
    let operands, operandRest = parseOperatorBody input
    {Header = header; Operands = operands}, operandRest
and parsePacket (input: bool array): Node*array<bool> =
    let header, headerRest = parseHeader input
    if header.Tag = 4 then (
        let lit, rest = parseLiteral header headerRest
        LiteralNode lit,rest
    ) else (
        let op, rest = parseOperator header headerRest
        OperatorNode op,rest
    )

let rec sumVersions (node: Node): int64 =
    match node with
    | LiteralNode lit -> lit.Header.Version
    | OperatorNode op -> op.Header.Version + ((Seq.map sumVersions op.Operands) |> Seq.sum)

exception InterpreterError of string

let rec interpret (node: Node): int64 =
    match node with
    | LiteralNode l -> l.Value
    | OperatorNode o -> (let op = match o.Header.Tag with 
        | 0L -> apply Seq.sum
        | 1L -> apply (Seq.reduce (fun p n -> p*n))
        | 2L -> apply Seq.min
        | 3L -> apply Seq.max
        | 5L -> apply (oneIf (fun vs -> Seq.head vs > (Seq.tail vs |> Seq.head)))
        | 6L -> apply (oneIf (fun vs -> Seq.head vs < (Seq.tail vs |> Seq.head)))
        | 7L -> apply (oneIf (fun vs -> Seq.head vs = (Seq.tail vs |> Seq.head)))
        | _ -> raise (InterpreterError (sprintf  "Unrecognized operation: %i" o.Header.Tag)) in
        op o.Operands)
and apply f (operands: Node seq): int64 =
    f (Seq.map interpret operands)
and oneIf (f: seq<int64> -> bool) (operands: int64 seq): int64 =
    if f operands then 1L else 0L

// let input = inputToBitArray "9C0141080250320F1802104A08"
// printfn "%A" input
// let operator, rest = parsePacket input
// printfn "%A" operator
// printfn "%i" (sumVersions operator)

// Map.iter (fun k v -> (
//     let input = inputToBitArray k
//     let packet, rest = parsePacket input
//     printfn "Expected %i, got %i" (sumVersions packet) v
// )) samples

let puzzleInput = inputToBitArray puzzle
let puzzleRoot, puzzleRest = parsePacket puzzleInput
printfn "%i" (sumVersions puzzleRoot)

printfn "%i" (interpret puzzleRoot)