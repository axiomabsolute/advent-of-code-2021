open System
open System.Text.RegularExpressions

let inputFile = "../../input"
// let inputFile = "../../sample"

let splitIntoBoards (lines: seq<String>) =
    lines |>
        Seq.filter (fun x -> not (String.IsNullOrWhiteSpace x)) |> // Remove blank lines
        Seq.map (fun x -> Regex.Split(x, "\s+") |> // Split each line on one or more whitespace (numbers are padded with spaces)
        Seq.map int) |> // Convert each split to an int
        Seq.chunkBySize 5 // Group by 5 lines for boards

let withIndex i e =
    (i, e)

let updateRows (call: int) (rows: Set<int> array) (boards: seq<seq<int> array>) =
    for (b, board) in boards |> Seq.mapi withIndex do
        for (r, row) in board |> Seq.mapi withIndex do 
            for (c, cell) in row |> Seq.mapi withIndex do
                let idx = (5 * b) + r
                if cell = call then Array.set rows idx ((Array.get rows idx).Add(cell))
    rows

let updateColumns (call: int) (cols: Set<int> array) (boards: seq<seq<int> array>) =
    for (b, board) in boards |> Seq.mapi withIndex do
        for (r, row) in board |> Seq.mapi withIndex do 
            for (c, cell) in row |> Seq.mapi withIndex do
                let idx = (5 * b) + c
                if cell = call then Array.set cols idx ((Array.get cols idx).Add(cell))
    cols

let checkWinner (boardStateRows: seq<Set<int>>) (boardStateCols: seq<Set<int>>) =
    let boardStates = Seq.append (Seq.mapi withIndex boardStateRows) (Seq.mapi withIndex boardStateCols)
    let fullBoards = boardStates |> Seq.filter (fun x -> (snd x).Count = 5) |> Seq.toList
    match fullBoards with
    | head :: _ -> Some head
    | _ -> None

let rec tick (rows: Set<int> array) (columns: Set<int> array) boards calls (called: Set<int>) (last: int) =
    let maybeWinner = checkWinner rows columns
    match maybeWinner with 
    | Some (idx, line) -> Some (idx, line, called, last)
    | None -> match calls with
        | head :: tail -> tick (updateRows head rows boards) (updateColumns head columns boards) boards tail (Set.add head called) head
        | _ -> None

let boardIndexFromIndex (index: int) =
    index / 5

let boardFromIndex (boards: seq<seq<int> array>) (index: int) =
    let boardIndex = boardIndexFromIndex index
    boards |> Seq.skip (boardIndex) |> Seq.head

// let printWinner (winningBoard: seq<int> array) (winningLine: Set<int>) =
let printWinner (boards: seq<seq<int> array>) (winningLine: Set<int>) (winningIndex: int) (called: Set<int>) (last: int) =
    let winningBoard = boardFromIndex boards winningIndex
    let unmarkedWinningBoardEntries = winningBoard |> Seq.concat |> Seq.filter (fun e -> not (called.Contains e))
    printfn "unmarked entries: %s" (String.Join(',', unmarkedWinningBoardEntries |> Seq.map Convert.ToString |> Seq.toArray))
    let unmarkedWinningBoardSum = Seq.sum unmarkedWinningBoardEntries
    let result = unmarkedWinningBoardSum * last
    printfn "Last: %i, Unmarked: %i" last unmarkedWinningBoardSum
    printfn "%i" result

[<EntryPoint>]
let main argv =
    let input = System.IO.File.ReadLines(inputFile) |> Seq.map (fun line -> line.Trim())

    let bingoNumbers = (Seq.head input).Split(',') |> Seq.map int
    // Each board is an array of 5 lines
    // Each line is a seq of ints
    let boards = input |> Seq.tail |> splitIntoBoards

    // Convert boards into functions
    // For each bingo number, execute all functions to update board states
    // Check aggregated board states for winning board and line
    let printAndReturnIdx boards line idx called last =
        printWinner boards line idx called last
        idx

    let printNoMatchesAndReturnNegative =
        printfn "No matches!"
        -1

    let removeItemAt collection idx =
       collection |> Seq.mapi withIndex |> Seq.filter (fun p -> idx <> (fst p)) |> Seq.map snd 

    // Prints winning boards in order
    let rec loop boards =
        if Seq.length boards = 0 then 0 else
        (
            let rows = seq { for _ in 1..(Seq.length boards) do for _ in 1..5 do Set.empty } |> Seq.toArray
            let columns = seq { for _ in 1..(Seq.length boards) do for _ in 1..5 do Set.empty } |> Seq.toArray
            let result = tick rows columns boards (bingoNumbers |> Seq.toList) Set.empty -1
            let idx = match result with
            | Some (idx, line, called, last) -> printAndReturnIdx boards line idx called last
            | None -> printNoMatchesAndReturnNegative
            let winningBoardIndex = boardIndexFromIndex idx
            let candidates = removeItemAt boards winningBoardIndex
            loop candidates
        )

    let _ = loop boards

    0 // return an integer exit code