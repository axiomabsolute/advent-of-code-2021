open System
open System.Text.RegularExpressions

exception ValueError of String

let inputFile = "../../input"
// let inputFile = "../../sample"

type Point = int * int

let isVerticalOrHorizontal (p1: Point) (p2: Point) =
    match (p1, p2) with
    | ((x1, _), (x2, _)) when x1 = x2 -> true
    | ((_, y1), (_, y2)) when y1 = y2 -> true
    | _ -> false

let lineToPoints (line: String) =
    let points = Regex.Split(line, "->") |>
        Seq.map (fun x -> x.Trim()) |>
        Seq.map (fun x -> Regex.Split(x, ",")) |> 
        Seq.map (fun pair -> match pair with
            | [| x; y|] -> (x |> int, y |> int)
            | _ -> raise(ValueError("Failed to parse: " + line))
        ) |>
        Seq.sort |>
        Seq.toList
    match points with
    | [a; b] -> (a, b)
    | _ -> raise(ValueError("Incorrect points"))

let genPoints (p1: Point) (p2: Point) =
    if isVerticalOrHorizontal p1 p2 then
        seq {
            for x in (fst p1)..(fst p2) do
                for y in (snd p1)..(snd p2) do
                    yield (x, y)
        }
    else
        // Diagonals are always +/- 45 deg; must determine whether incrasing or decreasing in each direction
        let xStep = match (p1, p2) with
        | ((x1, _), (x2, _)) when x1 < x2 -> 1
        | _ -> -1
        let yStep = match (p1, p2) with
        | ((_, y1), (_, y2)) when y1 < y2 -> 1
        | _ -> -1
        // Assumes other segments are always 45 deg diagonals, so x, y increase at same rate, either +/-1
        seq {
            for i in 0..((fst p2) - (fst p1)) do
                yield ((fst p1) + (i * xStep), (snd p1) + (i * yStep))
        }

let countOverlaps segments =
    let allPoints = segments |> Seq.collect (fun x -> genPoints (fst x) (snd x))
    let counts = allPoints |> Seq.countBy id
    let highCounts = counts |> Seq.filter (fun x -> (snd x) >= 2)
    highCounts |> Seq.length

[<EntryPoint>]
let main argv =
    let input = System.IO.File.ReadLines(inputFile) |> Seq.map (fun line -> line.Trim())

    let segments = input |> Seq.map lineToPoints
    let hvSegments = segments |> Seq.filter (fun x -> isVerticalOrHorizontal (fst x) (snd x))

    let hvCount = countOverlaps hvSegments
    let allCount = countOverlaps segments

    printfn "HV Count: %i" hvCount
    printfn "All Count: %i" allCount



    0 // return an integer exit code