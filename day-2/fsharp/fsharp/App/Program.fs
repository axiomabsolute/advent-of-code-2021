// Learn more about F# at http://docs.microsoft.com/dotnet/fsharp

open System

type Op = Up | Down | Forward | Bad

let lineToInstruction (line: String) =
    let parts = line.Split(" ")
    match parts with
    | [| "forward"; n |] -> (Forward, Int32.Parse n)
    | [| "down"; n |] -> (Down, Int32.Parse n)
    | [| "up"; n |] -> (Up, Int32.Parse n)
    | _ -> (Bad, 0)

let isHorizontal (instruction: Op * Int32) =
    match instruction with
    | (Forward, value) -> value
    | _ -> 0

let isVertical (instruction: Op * Int32) =
    match instruction with
    | (Up, value) -> -1 * value
    | (Down, value) -> value
    | _ -> 0

let rec interpret instructions aim x y =
    if Seq.isEmpty instructions then x*y else
    match instructions with
    | head :: tail -> (
        match head with
            | (Up, value) -> interpret tail (aim - value) x y
            | (Down, value) -> interpret tail (aim + value) x y
            | (Forward, value) -> interpret tail aim (x + value) (y + (aim * value))
            | _ -> 0
        )
    | _ -> 0

// Define a function to construct a message to print
let from whom =
    sprintf "from %s" whom

[<EntryPoint>]
let main argv =
    let filePath = "../../../input"
    let input = System.IO.File.ReadLines(filePath) |>
        Seq.map lineToInstruction

    let x = input |>
        Seq.map isHorizontal |>
        Seq.sum

    let y = input |>
        Seq.map isVertical |>
        Seq.sum

    printfn "%i" (x * y)

    printfn "%i" (interpret (List.ofSeq input) 0 0 0)

    0 // return an integer exit code