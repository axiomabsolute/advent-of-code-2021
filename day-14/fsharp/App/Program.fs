﻿open System.Text.RegularExpressions;
open System.Diagnostics;

let sampleText = """NNCB

CH -> B
HH -> N
CB -> H
NH -> C
HB -> C
HC -> B
HN -> C
NN -> C
BH -> H
NC -> B
NB -> B
BN -> B
BB -> N
BC -> B
CC -> N
CN -> C"""

let puzzleText = """PKHOVVOSCNVHHCVVCBOH

NO -> B
PV -> P
OC -> K
SC -> K
FK -> P
PO -> P
FC -> V
KN -> V
CN -> O
CB -> K
NF -> K
CO -> F
SK -> F
VO -> B
SF -> F
PB -> F
FF -> C
HC -> P
PF -> B
OP -> B
OO -> V
OK -> N
KB -> H
PN -> V
PP -> N
FV -> S
BO -> O
HN -> C
FP -> F
BP -> B
HB -> N
VC -> F
PC -> V
FO -> O
OH -> S
FH -> B
HK -> B
BC -> F
ON -> K
FN -> N
NN -> O
PH -> P
KS -> H
HV -> F
BK -> O
NP -> S
CC -> H
KV -> V
NB -> C
NS -> S
KO -> V
NK -> H
HO -> C
KC -> P
VH -> C
VK -> O
CP -> K
BS -> N
BB -> F
VV -> K
SH -> O
SO -> N
VF -> K
NV -> K
SV -> O
NH -> C
VS -> N
OF -> N
SP -> C
HP -> O
NC -> V
KP -> B
KH -> O
SN -> S
CS -> N
FB -> P
OB -> H
VP -> B
CH -> O
BF -> B
PK -> S
CF -> V
CV -> S
VB -> P
CK -> H
PS -> N
SS -> C
OS -> P
OV -> F
VN -> V
BV -> V
HF -> B
FS -> O
BN -> K
SB -> N
HH -> S
BH -> S
KK -> H
HS -> K
KF -> V"""

exception InputException of string

type Board = array<char>
type PairInsertionRule = char*char
type PairInsertionRules = Map<char array,char array array>
type Puzzle = array<char>*PairInsertionRules

let INPUT_PATTERN = "([A-Z][A-Z]) -> ([A-Z])"

let printTime (msg: string) (s: Stopwatch): unit =
    printfn "%s %f" msg (s.Elapsed.TotalMilliseconds)

let normalizeNewlines (text: string) : string =
    text
        .Replace("\r\n", "\r")
        .Replace("\n\r", "\r")
        .Replace("\r", "\n")

let parse (text: string): Puzzle =
    let lines = (normalizeNewlines text).Split("\n")
    let initialState = lines.[0].Trim()
    let insertionRules = List.ofArray lines |> List.skip 2 |> List.map (fun line -> (
        match Regex(INPUT_PATTERN).Match(line).Groups |> List.ofSeq with 
        | [_; f; t] -> f.Value |> Array.ofSeq, [| [|f.Value[0]; t.Value[0]|]; [| t.Value[0]; f.Value[1] |] |]
        | _ -> raise (InputException("Bad Pair Insertion Rule"))
    ))
    initialState |> Array.ofSeq, insertionRules |> Map.ofSeq

let printBoard (board: Board) (msg: string): string =
    sprintf "Board %s - %s" msg (board |> System.String.Concat)

let expandPair (rules: PairInsertionRules) (pair: char array) (count: int64): Map<char array,int64> =
    let rule = rules[pair]
    Map.empty.Add(rule[0], count).Add(rule[1], count)

let mergeMaps (f : 'a -> 'b * 'b -> 'b) (a : Map<'a, 'b>) (b : Map<'a, 'b>) =
    Map.fold (fun s k v ->
        match Map.tryFind k s with
        | Some v' -> Map.add k (f k (v, v')) s
        | None -> Map.add k v s) a b

let expandLine (rules: PairInsertionRules) (counts: Map<char array,int64>): Map<char array,int64> =
    let updates = Seq.map (fun kv -> expandPair rules (fst kv) (snd kv)) (Map.toSeq counts)
    let folder = mergeMaps (fun k (v1, v2) -> v1 + v2)
    Seq.reduce folder updates

let partOne (text: string) (iterations: int): int64 =
    let initialState, rules = parse text
    let counts = Seq.windowed 2 initialState |> Seq.countBy id |> Seq.map (fun kv -> (fst kv, int64(snd kv))) |> Map.ofSeq
    let folder = expandLine rules
    let res = Seq.fold (fun s _ -> folder s) counts (seq { 1 .. iterations})
    let mapMerger = mergeMaps (fun k (v1, v2) -> v1 + v2)
    let letterCounts = (Map.toSeq res) |> Seq.map (fun (k,v) -> Map.empty.Add(k[0], v)) |> Seq.reduce mapMerger
    let lastLetter = Array.last initialState
    let letterCountsWithExtraTail = letterCounts.Add(lastLetter, letterCounts[lastLetter] + 1L)
    let values = Map.values letterCountsWithExtraTail |> Seq.sort |> Array.ofSeq
    (Array.last values) - (Array.head values)

let stopWatch = Stopwatch.StartNew()
let iterations = 40

// printfn "Sample %i iter part 1: %i" iterations (partOne sampleText iterations)
// printTime "Sample time" stopWatch

printfn "Puzzle %i iter part 1: %i" 10 (partOne puzzleText 10)

printfn "Puzzle %i iter part 1: %i" iterations (partOne puzzleText iterations)
printTime "Puzzle time" stopWatch